<nav class="navbar navbar-expand-lg navbar-light bg-white shadow-sm">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="navbar-brand pl-5">
        <a href="/"><img src="/img/pcaLogo.png"></a>
    </div>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <!-- Right Side Of Navbar -->
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/">Home</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/about">About</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/event">Events</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/red-sails">Red Sails Festival</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/news">News</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contact-us">Contact Us</a>
            </li>
            <!-- Authentication Links -->
            @guest
                <li class="nav-item">
                    <a class="nav-link" href="/login">{{ __('Login') }}</a>
                </li>
            @else
                @if (Auth::user()->can('manage-users'))
                    <li class="nav-item">
                        <a class="nav-link" href="/admin/dashboard">{{ __('Go to account') }}</a>
                    </li>

                @elseif(Auth::user()->can('manage-events'))
                    <li class="nav-item">
                        <a class="nav-link" href="/events/index">{{ __('Go to account') }}</a>
                    </li>

                @elseif(Auth::user()->can('manage-news'))
                    <li class="nav-item">
                        <a class="nav-link" href="/news/index">{{ __('Go to account') }}</a>
                    </li>

                @elseif(Auth::user()->can('view-policy'))
                    <li class="nav-item">
                        <a class="nav-link" href="/user/events">{{ __('Go to account') }}</a>
                    </li>
                @endif
                    <li class="nav-item">
                        <a class="nav-link" href="/logout"
                           onclick="event.preventDefault();
                                   document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    </li>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            @endguest
        </ul>
    </div>
</nav>
