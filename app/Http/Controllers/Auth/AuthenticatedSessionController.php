<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     */
    public function create()
    {
        return view('auth.login');
//        return Inertia::render('Auth/Login', [
//            'canResetPassword' => Route::has('password.request'),
//            'status' => session('status'),
//        ]);
    }

    /**
     * Handle an incoming authentication request.
     */
    public function store(LoginRequest $request)
    {
        $request->authenticate();

        $request->session()->regenerate();

        $userID = Auth::user()->id;
        $user = Auth::user();
        $query = DB::table('users')
            ->select('profile_set')
            ->where('users.id', $userID)->first();

        if($query->profile_set == 0) {
            return redirect('/user/profile/create');
        }
        if($user->hasRole('Admin')) {
            return redirect('/admin/dashboard');
        }
        if($user->hasRole('Event Manager')) {
            return redirect('/event-manager/index');
        }
        if($user->hasRole('Member')) {
            return redirect('/user/events');
        }
        if($user->hasRole('Author')) {
            return redirect('/author');
        }
        //return redirect()->intended(route('dashboard', absolute: false));
    }

    /**
     * Destroy an authenticated session.
     */
    public function destroy(Request $request): RedirectResponse
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
